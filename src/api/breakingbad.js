const baseUrl = 'https://www.breakingbadapi.com/api/';


const getBreakingBad = () => {
    return fetch(`${baseUrl}characters` , {
        method: 'GET',
        cors: true,
    }).then((response) => response.json());
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    getBreakingBad
};