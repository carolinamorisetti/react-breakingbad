const home = '/app';
const breakingBad = `${home}breakingBad`;
const breakingList = `${breakingBad}/:id`;

export default {
    home,
    breakingBad,
    breakingList
}