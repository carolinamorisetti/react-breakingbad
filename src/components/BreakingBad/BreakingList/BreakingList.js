import React from 'react';

import './styles.css';
 

import breakingBadApi from '../../../api/breakingbad';

class BreakingList extends React.Component {
    state = {
        breakingBad: [],
        isLoading: false,
    };
    componentDidMount() {
        this.setState({ isLoading: true});
        breakingBadApi.getBreakingBad().then(data => {
            this.setState({
                breakingBad: data,
                isLoading: false,
            })
        });
    }
    render() {
        const { isLoading, breakingBad } = this.state;
        return (
            <div className="box">
                {isLoading ? <h3>Cargando...</h3> : null}
                {breakingBad.map((breakingBad) => (
                        <div key={breakingBad.id} className="card">
                            <h5>{breakingBad.name}</h5>
                            <img src={breakingBad.img} alt={breakingBad.name} style={{width: '45px'}}/>
                        </div>
                ))}

            </div>
        );
    }

}

export default BreakingList;